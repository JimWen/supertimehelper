// DlgAlarm.cpp : 实现文件
//

#include "stdafx.h"
#include "SuperTimeHelper.h"
#include "DlgAlarm.h"


// CDlgAlarm 对话框

IMPLEMENT_DYNAMIC(CDlgAlarm, CDialog)

CDlgAlarm::CDlgAlarm(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgAlarm::IDD, pParent)
	, m_info(_T(""))
{

}

CDlgAlarm::~CDlgAlarm()
{
}

void CDlgAlarm::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDS_INFO, m_info);
}


BEGIN_MESSAGE_MAP(CDlgAlarm, CDialog)
END_MESSAGE_MAP()

void CDlgAlarm::SetInfo( CString info , UINT timeLazyMinute)
{
	m_info = info;
	m_timeLazyMinute = timeLazyMinute;
}


// CDlgAlarm 消息处理程序

BOOL CDlgAlarm::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  在此添加额外的初始化

	//对于选择无懒人模式的不显示延迟按钮
	if (0 == m_timeLazyMinute)
	{
		GetDlgItem(IDCANCEL)->ShowWindow(FALSE);
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常: OCX 属性页应返回 FALSE
}
