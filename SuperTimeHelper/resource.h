//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by SuperTimeHelper.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_SUPERTIMEHELPER_DIALOG      102
#define IDR_MAINFRAME                   128
#define IDR_MAINMENU                    129
#define IDR_TOOLBAR_MENU                145
#define IDI_ADD                         147
#define IDI_COPY                        148
#define IDI_DELETE                      149
#define IDI_ICON3                       150
#define IDI_EDIT                        150
#define IDI_ALARM                       151
#define IDI_ALERT                       152
#define IDI_ALARMSHUT                   152
#define IDD_ADD                         154
#define IDD_ALARM                       155
#define IDI_RESTART                     156
#define IDD_HOTKEY                      156
#define IDD_ALERT1                      157
#define IDI_SHUTDOWN                    157
#define IDI_LOGOFF                      158
#define IDI_ALARMSTART                  159
#define IDR_TRAY_MENU                   160
#define IDC_TIMELIST                    1004
#define IDB_ADD                         1005
#define IDB_EDIT                        1006
#define IDC_DATE                        1006
#define IDB_DEL                         1007
#define IDC_TIME                        1007
#define IDB_COPY                        1008
#define IDC_TIMETYPE                    1008
#define IDC_TIMEFREQ                    1009
#define IDE_INFO                        1010
#define IDS_INFO                        1011
#define IDC_LAZYMINUTE                  1011
#define IDC_HOTKEY1                     1013
#define IDC_HOTKEY                      1013
#define ID_32771                        32771
#define ID_32772                        32772
#define ID_32773                        32773
#define ID_32774                        32774
#define ID_32775                        32775
#define ID_32776                        32776
#define ID_32777                        32777
#define ID_32778                        32778
#define ID_32779                        32779
#define ID_32780                        32780
#define ID_32781                        32781
#define ID_32782                        32782
#define ID_32783                        32783
#define ID_32784                        32784
#define ID_32785                        32785
#define ID_32786                        32786
#define ID_32787                        32787
#define IDM_OPENFILE                    32788
#define IDM_SAVEFILE                    32789
#define IDM_READ                        32790
#define IDM_EXPORT                      32791
#define IDM_EXIT                        32792
#define ID_32793                        32793
#define IDM_HOTKEY                      32794
#define IDM_HIDE                        32795
#define IDM_AUTORUN                     32796
#define IDM_WEBSITE                     32797
#define IDM_ABOUT                       32798
#define ID_Menu                         32799
#define IDM_MAIN                        32800

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        161
#define _APS_NEXT_COMMAND_VALUE         32801
#define _APS_NEXT_CONTROL_VALUE         1014
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
