// DlgAdd.cpp : 实现文件
//

#include "stdafx.h"
#include "SuperTimeHelper.h"
#include "SuperTimeHelperDlg.h"
#include "DlgAdd.h"


// CDlgAdd 对话框

IMPLEMENT_DYNAMIC(CDlgAdd, CDialog)

CDlgAdd::CDlgAdd(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgAdd::IDD, pParent)
	, m_windowCaption(_T(""))
	, m_date(0)
	, m_time(0)
	, m_timeType(0)
	, m_timeFreq(0)
	, m_timeInfo(_T(""))
	, m_timeLazyMinute(10)
{

}

CDlgAdd::CDlgAdd( CString wndCaption, 
				  CTime date, CTime time, 
				  int timeLazyMinute/*=10*/, 
				  int timeType/*=0*/, int timeFreq/*=0*/, 
				  CString timeInfo/*=TEXT("")*/, CWnd* pParent /*=NULL*/): CDialog(CDlgAdd::IDD, pParent)
{
	m_windowCaption = wndCaption;
	m_date = date;
	m_time = time;
	m_timeLazyMinute = timeLazyMinute;
	m_timeType = timeType;
	m_timeFreq = timeFreq;
	m_timeInfo = timeInfo;
}

CDlgAdd::CDlgAdd( CString wndCaption, 
				  int timeLazyMinute/*=10*/, 
				  int timeType/*=0*/, int timeFreq/*=0*/, 
				  CString timeInfo/*=TEXT("")*/, CWnd* pParent /*=NULL*/): CDialog(CDlgAdd::IDD, pParent)
{
	m_windowCaption = wndCaption;
	m_date = CTime::GetCurrentTime();
	m_time = CTime::GetCurrentTime();
	m_timeLazyMinute = timeLazyMinute;
	m_timeType = timeType;
	m_timeFreq = timeFreq;
	m_timeInfo = timeInfo;
}

CDlgAdd::CDlgAdd( CString wndCaption, const CTimeObject *pObject, CWnd* pParent /*= NULL*/ ): CDialog(CDlgAdd::IDD, pParent)
{
	m_windowCaption = wndCaption;
	m_date = pObject->m_time;
	m_time = pObject->m_time;
	m_timeLazyMinute = pObject->m_timeLazyMinute;
	m_timeType = pObject->m_timeType;
	m_timeFreq = pObject->m_timeFreq;
	m_timeInfo = pObject->m_info;
}

CDlgAdd::~CDlgAdd()
{
}

void CDlgAdd::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_DateTimeCtrl(pDX, IDC_DATE, m_date);
	DDX_DateTimeCtrl(pDX, IDC_TIME, m_time);
	DDX_CBIndex(pDX, IDC_TIMETYPE, m_timeType);
	DDX_CBIndex(pDX, IDC_TIMEFREQ, m_timeFreq);
	DDX_Text(pDX, IDE_INFO, m_timeInfo);
	DDX_Text(pDX, IDC_LAZYMINUTE, m_timeLazyMinute);
	DDV_MinMaxInt(pDX, m_timeLazyMinute, 0, 60);
	DDV_MaxChars(pDX, m_timeInfo, 255);
}


BEGIN_MESSAGE_MAP(CDlgAdd, CDialog)
	ON_CBN_SELCHANGE(IDC_TIMETYPE, &CDlgAdd::OnCbnSelchangeTimetype)
	ON_BN_CLICKED(IDOK, &CDlgAdd::OnBnClickedOk)
END_MESSAGE_MAP()


// CDlgAdd 消息处理程序

BOOL CDlgAdd::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  在此添加额外的初始化
	//设置Windows标题
	SetWindowText(m_windowCaption);

	//设置频率Combo
	((CComboBox *)GetDlgItem(IDC_TIMEFREQ))->InsertString(0, TEXT("每星期"));
	((CComboBox *)GetDlgItem(IDC_TIMEFREQ))->InsertString(1, TEXT("工作日"));
	((CComboBox *)GetDlgItem(IDC_TIMEFREQ))->InsertString(2, TEXT("每天"));
	((CComboBox *)GetDlgItem(IDC_TIMEFREQ))->InsertString(3, TEXT("每小时"));

	//设置类型Combo
	((CComboBox *)GetDlgItem(IDC_TIMETYPE))->InsertString(0, TEXT("闹钟"));
	((CComboBox *)GetDlgItem(IDC_TIMETYPE))->InsertString(1, TEXT("定时关机"));
	((CComboBox *)GetDlgItem(IDC_TIMETYPE))->InsertString(2, TEXT("定时注销"));
	((CComboBox *)GetDlgItem(IDC_TIMETYPE))->InsertString(3, TEXT("定时重启"));
	((CComboBox *)GetDlgItem(IDC_TIMETYPE))->InsertString(4, TEXT("关机时提醒"));
	((CComboBox *)GetDlgItem(IDC_TIMETYPE))->InsertString(5, TEXT("开机时提醒"));

	//不同定时类型时可用和不可用控件
	InitControlState();

	UpdateData(FALSE);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常: OCX 属性页应返回 FALSE
}

void CDlgAdd::OnCbnSelchangeTimetype()
{
	// TODO: 在此添加控件通知处理程序代码
	UpdateData(TRUE);

	//不同定时类型时可用和不可用控件
	InitControlState();
}

void CDlgAdd::OnBnClickedOk()
{
	// TODO: 在此添加控件通知处理程序代码
	UpdateData(TRUE);

	//在列表中，最多只能存在一个开机提醒和一个关机提醒,这是因为关机或开机时的所有提醒是同时响应的
	if (((CSuperTimeHelperDlg *)GetParent())->m_timeListBox.m_pShutObject != NULL && 
		TIMETYPE_ALARMONSHUT == (ENUM_TIMETYPE)m_timeType && 
		TEXT("编辑定时") != m_windowCaption)
	{
		MessageBox(TEXT("只能添加一个关机提醒\n如有需要请在现有的关机提醒中添加提醒信息"), TEXT("警告"));
		return ;
	}
	else if(((CSuperTimeHelperDlg *)GetParent())->m_timeListBox.m_pStartObject != NULL && 
		TIMETYPE_ALARMONSTART == (ENUM_TIMETYPE)m_timeType &&
		TEXT("编辑定时") != m_windowCaption)
	{
		MessageBox(TEXT("只能添加一个开机提醒\n如有需要请在现有的关机提醒中添加提醒信息"), TEXT("警告"));
		return ;
	}

	//如果选择工作日模式,会自动将时间调整至最近一次工作日
	if (m_timeFreq == 1 && (m_date.GetDayOfWeek() == 7))
	{
		m_date += CTimeSpan(2, 0, 0, 0);
	}
	else if(m_timeFreq == 1 && (m_date.GetDayOfWeek() == 1))
	{
		m_date += CTimeSpan(1, 0, 0, 0);
	}
	UpdateData(FALSE);

	OnOK();
}

void CDlgAdd::InitControlState()
{
	switch (m_timeType)
	{
	case 0:
		GetDlgItem(IDC_TIME)->EnableWindow(TRUE);
		GetDlgItem(IDC_LAZYMINUTE)->EnableWindow(TRUE);
		GetDlgItem(IDE_INFO)->EnableWindow(TRUE);
		break;
	case 1:
	case 2:
	case 3:
		GetDlgItem(IDC_TIME)->EnableWindow(TRUE);
		GetDlgItem(IDC_LAZYMINUTE)->EnableWindow(FALSE);
		GetDlgItem(IDE_INFO)->EnableWindow(FALSE);
		break;
	case 4:
	case 5:
		GetDlgItem(IDC_TIME)->EnableWindow(FALSE);
		GetDlgItem(IDC_LAZYMINUTE)->EnableWindow(FALSE);
		GetDlgItem(IDE_INFO)->EnableWindow(TRUE);
		break;
	}
}
