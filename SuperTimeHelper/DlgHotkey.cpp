// DlgHotkey.cpp : 实现文件
//

#include "stdafx.h"
#include "SuperTimeHelper.h"
#include "DlgHotkey.h"


// CDlgHotkey 对话框

IMPLEMENT_DYNAMIC(CDlgHotkey, CDialog)

CDlgHotkey::CDlgHotkey(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgHotkey::IDD, pParent)
{

}

CDlgHotkey::CDlgHotkey( int nHotkey, WORD wModifer, CWnd* pParent /*= NULL*/ )
	: CDialog(CDlgHotkey::IDD, pParent)
	,m_nHotkey(nHotkey)
	,m_wModifer(wModifer)

{

}

CDlgHotkey::~CDlgHotkey()
{
}

void CDlgHotkey::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_HOTKEY, m_HotKeyCtrl);
}


BEGIN_MESSAGE_MAP(CDlgHotkey, CDialog)
END_MESSAGE_MAP()


// CDlgHotkey 消息处理程序

BOOL CDlgHotkey::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  在此添加额外的初始化
	m_HotKeyCtrl.SetHotKey(m_nHotkey, m_wModifer);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常: OCX 属性页应返回 FALSE
}

void CDlgHotkey::OnOK()
{
	// TODO: 在此添加专用代码和/或调用基类
	m_HotKeyCtrl.GetHotKey((WORD &)m_nHotkey, m_wModifer);

	CDialog::OnOK();
}
