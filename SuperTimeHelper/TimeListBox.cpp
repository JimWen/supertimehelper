// TimeListBox.cpp : 实现文件
//

#include "stdafx.h"
#include "SuperTimeHelper.h"
#include "TimeListBox.h"
#include <tchar.h>


// CTimeListBox

IMPLEMENT_DYNAMIC(CTimeListBox, CListBox)

CTimeListBox::CTimeListBox()
{
	m_pShutObject = NULL;
	m_pStartObject = NULL;
}

CTimeListBox::~CTimeListBox()
{
}


BEGIN_MESSAGE_MAP(CTimeListBox, CListBox)
END_MESSAGE_MAP()



// 重绘函数


void CTimeListBox::DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct)
{

	// TODO:  添加您的代码以绘制指定项
	//获得DC并保存初始化DC，操作完成后必须恢复原始DC
	TRACE("进入DrawItem");

	if (0 == GetCount())
	{
		return ;
	}

	m_CDC.Attach(lpDrawItemStruct->hDC);
	m_CDC.SaveDC();

	//保存默认的背景颜色和字体颜色
	COLORREF crOldTextColor = m_CDC.GetTextColor();
	COLORREF crOldBkColor = m_CDC.GetBkColor();

	/*获得用户绘制的区域*/
	CRect rectListBoxItem = lpDrawItemStruct->rcItem;

	CRect rectIcon;
	rectIcon.left = 10;
	rectIcon.top = rectListBoxItem.top + (LISTBOX_HEIGHT - 32)/2;//保证32大小的Icon处于正中
	rectIcon.right = rectIcon.left + 32;
	rectIcon.bottom = rectIcon.top + 32;

	CRect rectTextTime, rectTextFreq, rectTextCount, rectTextInfo;
	TEXTMETRIC tm;
	m_CDC.GetTextMetrics(&tm);
	rectTextTime = rectListBoxItem;
	rectTextTime.left = rectIcon.right + 20;
	rectTextTime.bottom = rectTextTime.top + tm.tmHeight + tm.tmExternalLeading + 5;
	rectTextTime.right = rectTextTime.right - tm.tmAveCharWidth*28;

	rectTextFreq = rectListBoxItem;
	rectTextFreq.left = rectTextTime.right + 5;
	rectTextFreq.bottom = rectTextTime.top + tm.tmHeight + tm.tmExternalLeading + 5;
	rectTextFreq.right = rectListBoxItem.right - tm.tmAveCharWidth*20;

	rectTextCount = rectListBoxItem;
	rectTextCount.left = rectTextFreq.right + 5;
	rectTextCount.bottom = rectTextTime.top + tm.tmHeight + tm.tmExternalLeading + 5;

	rectTextInfo = rectListBoxItem;
	rectTextInfo.left = rectIcon.right + 20;
	rectTextInfo.top = rectTextTime.bottom + 5;
	CString csTemp;

	//获得用户绘制的数据
	CTimeObject *pListDataItem = (CTimeObject *)GetItemData(lpDrawItemStruct->itemID);

	// If the user has selected an item from the list box, or the selection has been changed
	// draw the item of the list box and fill it with the color of selected item
	if ((lpDrawItemStruct->itemAction | ODA_SELECT) &&
		(lpDrawItemStruct->itemState & ODS_SELECTED))

	{	
		// Fill the item rect with the highlight blue color
		m_CDC.FillSolidRect(rectListBoxItem,::GetSysColor(COLOR_HIGHLIGHT));		
		// Set the color of the background of the text rect
		m_CDC.SetBkColor(::GetSysColor(COLOR_HIGHLIGHT));
		// Set the color of the text
		m_CDC.SetTextColor(::GetSysColor(COLOR_HIGHLIGHTTEXT));	
	}
	else
	{
		// Fill the item rect with white
		m_CDC.FillSolidRect(&lpDrawItemStruct->rcItem, crOldBkColor);
	}

	//绘制图标
	switch (pListDataItem->m_timeType)
	{
	case TIMETYPE_ALARM:
		m_CDC.DrawIcon(rectIcon.left, rectIcon.top, AfxGetApp()->LoadIcon(IDI_ALARM));
		break;
	case TIMETYPE_SHUTDOWN:
		m_CDC.DrawIcon(rectIcon.left, rectIcon.top, AfxGetApp()->LoadIcon(IDI_SHUTDOWN));
		break;
	case TIMETYPE_LOGOFF:
		m_CDC.DrawIcon(rectIcon.left, rectIcon.top, AfxGetApp()->LoadIcon(IDI_LOGOFF));
		break;
	case TIMETYPE_RESTART:
		m_CDC.DrawIcon(rectIcon.left, rectIcon.top, AfxGetApp()->LoadIcon(IDI_RESTART));
		break;
	case TIMETYPE_ALARMONSHUT:
		m_CDC.DrawIcon(rectIcon.left, rectIcon.top, AfxGetApp()->LoadIcon(IDI_ALARMSHUT));
		break;
	case TIMETYPE_ALARMONSTART:
		m_CDC.DrawIcon(rectIcon.left, rectIcon.top, AfxGetApp()->LoadIcon(IDI_ALARMSTART));
		break;
	}

	/***绘制内容***/
	//绘制时间
	csTemp.Format(TEXT("%d年%d月%d日 %d时%d分%d秒"), 
		pListDataItem->m_time.GetYear(),
		pListDataItem->m_time.GetMonth(),
		pListDataItem->m_time.GetDay(),
		pListDataItem->m_time.GetHour(),
		pListDataItem->m_time.GetMinute(),
		pListDataItem->m_time.GetSecond());
	m_CDC.DrawText(csTemp, rectTextTime, DT_LEFT | DT_SINGLELINE | DT_VCENTER);

	//绘制频率
	switch (pListDataItem->m_timeFreq)
	{
	case TIMEFREQ_WEEK:
		m_CDC.DrawText(TEXT("每星期"), rectTextFreq, DT_LEFT | DT_SINGLELINE | DT_VCENTER);
		break;
	case TIMEFREQ_WEEKDAY:
		m_CDC.DrawText(TEXT("工作日"), rectTextFreq, DT_LEFT | DT_SINGLELINE | DT_VCENTER);
		break;
	case TIMEFREQ_DAY:
		m_CDC.DrawText(TEXT("每天"), rectTextFreq, DT_LEFT | DT_SINGLELINE | DT_VCENTER);
		break;
	case TIMEFREQ_HOUR:
		m_CDC.DrawText(TEXT("每小时"), rectTextFreq, DT_LEFT | DT_SINGLELINE | DT_VCENTER);
		break;
	}

	//绘制次数
	if (pListDataItem->m_timeCount != 0)
	{
		csTemp.Format(TEXT("延时%d分钟"), pListDataItem->m_timeCount*pListDataItem->m_timeLazyMinute);
		m_CDC.DrawText(csTemp, rectTextCount, DT_LEFT | DT_SINGLELINE | DT_VCENTER);
	}

	//绘制信息
	m_CDC.DrawText(pListDataItem->m_info, rectTextInfo, DT_LEFT | DT_SINGLELINE | DT_VCENTER);

	// If the List box has the focus,
	// draw around the first item the focus rect, which is a black dotted rectangle
	if((lpDrawItemStruct->itemAction | ODA_FOCUS) &&
		(lpDrawItemStruct->itemState & ODS_FOCUS))

	{
		m_CDC.DrawFocusRect(rectListBoxItem);
	}

	//恢复DC和释放DC绑定
	m_CDC.RestoreDC(-1);
	m_CDC.Detach();
}

void CTimeListBox::MeasureItem(LPMEASUREITEMSTRUCT lpMeasureItemStruct)
{

	// TODO:  添加您的代码以确定指定项的大小
	TRACE("进入MeasureItem");

	lpMeasureItemStruct->itemHeight = LISTBOX_HEIGHT;
}

int CTimeListBox::CompareItem(LPCOMPAREITEMSTRUCT lpCompareItemStruct)
{

	// TODO:  添加您的代码以确定指定项的排序顺序
	// 返回 -1 表示项 1 排在项 2 之前
	// 返回 0 表示项 1 和项 2 顺序相同
	// 返回 1 表示项 1 排在项 2 之后
	TRACE("进入CompareItem");

	CTimeObject *pObject1 = (CTimeObject*) lpCompareItemStruct->itemData1;
	CTimeObject *pObject2 = (CTimeObject*) lpCompareItemStruct->itemData2;
	
	return (pObject1->m_time + CTimeSpan(0, 0, pObject1->m_timeCount*pObject1->m_timeLazyMinute, 0)) 
			<= (pObject2->m_time + CTimeSpan(0, 0, pObject2->m_timeCount*pObject2->m_timeLazyMinute, 0)) 
			? -1 : 1;
}

void CTimeListBox::DeleteItem(LPDELETEITEMSTRUCT lpDeleteItemStruct)
{
	// TODO: 在此添加专用代码和/或调用基类
	TRACE("进入DeleteItem");

	//获得用户绘制的数据
	CTimeObject *pObject = (CTimeObject *)GetItemData(lpDeleteItemStruct->itemID);

	//更新是否有开关机提醒消息
	if(TIMETYPE_ALARMONSHUT == pObject->m_timeType)
	{
		m_pShutObject = NULL;
	}
	else if (TIMETYPE_ALARMONSTART == pObject->m_timeType)
	{
		m_pStartObject = NULL;
	}

	delete pObject;//删除附带的数据

	CListBox::DeleteItem(lpDeleteItemStruct);
}

//操作函数

void CTimeListBox::AddObject( const CTime time, const UINT timeLazyMinute, 
							  const ENUM_TIMEFREQ timeFreq, const ENUM_TIMETYPE timeType, 
							  const CString info, 
							  const UINT timeCount/*=0*/ )
{
	TRACE("进入AddObject");

	//创建时间事件对象
	CTimeObject *pObject = new CTimeObject(time, timeLazyMinute, timeFreq, timeType, info, timeCount);

	//更新是否有开关机提醒消息
	if(TIMETYPE_ALARMONSHUT == pObject->m_timeType)
	{
		m_pShutObject = pObject;
	}
	else if (TIMETYPE_ALARMONSTART == pObject->m_timeType)
	{
		m_pStartObject = pObject;
	}

	//新增一个索引并指向新建的时间事件对象
	UINT index = AddString((LPCTSTR)pObject);
	SetCurSel(index);
}

void CTimeListBox::AddObjectFromStr(CString csData )
{
	CTime time;
	UINT timeLazyMinute;
	ENUM_TIMEFREQ timeFreq; 
	ENUM_TIMETYPE timeType;
	CString info; 
	UINT timeCount;

	TCHAR szBuffer[255]=TEXT("\0");	//Info最多输入255个字符

	_stscanf (csData.GetBuffer(0), TEXT("time:%I64d lazy:%u freq:%u type:%u count:%u info:%s"),
		  &time,
		  &timeLazyMinute,
		  &timeFreq,
		  &timeType,
		  &timeCount,
		  szBuffer);
	info = szBuffer;

	AddObject(time, timeLazyMinute, timeFreq, timeType, info, timeCount);
}

void CTimeListBox::DeleteObject( UINT nIndex )
{
	TRACE("进入DeleteObject");

	//删除索引触发DeleteItem从而删除附加数据
	this->DeleteString(nIndex);
	SetCurSel(nIndex-1);
}

void CTimeListBox::ModifyObject( UINT nIndex, 
								 const CTime time, const UINT timeLazyMinute, 
								 const ENUM_TIMEFREQ timeFreq, const ENUM_TIMETYPE timeType, 
								 const CString info, 
								 const UINT timeCount )
{
	TRACE("进入ModifyObject");

	DeleteObject(nIndex);
	AddObject(time, timeLazyMinute, timeFreq, timeType, info, timeCount);
}

void CTimeListBox::ReadFromFile(JWIni &m_configIni)
{
	CString csTemp;
	int nCount;

	//清空当前列表
	ResetContent();

	//获得定时列表个数
	nCount = m_configIni.GetInt(TEXT("Data"), TEXT("Count"));

	//读入数据创建列表
	for (int i = 0; i < nCount; i++)
	{
		csTemp.Format(TEXT("data%d"), i);
		AddObjectFromStr(m_configIni.GetString(TEXT("Data"), csTemp));
	}
}

void CTimeListBox::ExportToFile( JWIni &m_configIni )
{
	CTimeObject *pObject;
	CString csExport;
	CString csTemp;

	//遍历节点数据写到配置文件中
	m_configIni.SetSection(TEXT("Data"), TEXT("\0\0"));//清空原先数据
	m_configIni.SetInt(TEXT("Data"), TEXT("Count"), GetCount());
	for (int i = 0; i<GetCount(); i++ )
	{
		pObject = (CTimeObject *)(GetItemData(i));

		csTemp.Format(TEXT("data%d"), i);
		m_configIni.SetString(TEXT("Data"), csTemp, pObject->ToString());
	}
}

int CTimeListBox::FindTheTypeIndex( ENUM_TIMETYPE timeType )
{
	int i;
	for (i=0; i<GetCount(); i++)
	{
		if (timeType == ((CTimeObject *)(GetItemData(i)))->m_timeType)
		{
			return i;
		}
	}

	return -1;//没有找到则返回-1
}
