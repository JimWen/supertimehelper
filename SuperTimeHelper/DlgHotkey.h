#pragma once
#include "afxcmn.h"


// CDlgHotkey 对话框

class CDlgHotkey : public CDialog
{
	DECLARE_DYNAMIC(CDlgHotkey)

public:
	CDlgHotkey(CWnd* pParent = NULL);   // 标准构造函数
	CDlgHotkey(int nHotkey, WORD wModifer, CWnd* pParent = NULL);   // 参数构造函数
	virtual ~CDlgHotkey();

public:
	int m_nHotkey;
	WORD m_wModifer;

// 对话框数据
	enum { IDD = IDD_HOTKEY };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	CHotKeyCtrl m_HotKeyCtrl;
	virtual BOOL OnInitDialog();
protected:
	virtual void OnOK();
};
