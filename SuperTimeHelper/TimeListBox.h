#pragma once


// CTimeListBox
#include "TimeObject.h"

#define LISTBOX_HEIGHT	50

class CTimeListBox : public CListBox
{
	DECLARE_DYNAMIC(CTimeListBox)

public:

	CTimeObject *m_pShutObject;			//关机提示消息指针,NULL表示没有
	CTimeObject *m_pStartObject;		//开机提示消息指针,NULL表示没有

	CTimeListBox();
	virtual ~CTimeListBox();
	void AddObject( const CTime time, const UINT timeLazyMinute,
					const ENUM_TIMEFREQ timeFreq, const ENUM_TIMETYPE timeType, 
					const CString info,
					const UINT timeCount=0);
	void AddObjectFromStr(CString csData);
	void DeleteObject(UINT nIndex);
	void ModifyObject(UINT nIndex, 
					  const CTime time, const UINT timeLazyMinute, 
					  const ENUM_TIMEFREQ timeFreq, const ENUM_TIMETYPE timeType, 
					  const CString info,
					  const UINT timeCount);
	void ReadFromFile(JWIni &m_configIni);
	void ExportToFile(JWIni &m_configIni);
	int FindTheTypeIndex(ENUM_TIMETYPE timeType);//遍历获得指定类型的第一个对象的Index

private:
	CDC m_CDC;


protected:
	DECLARE_MESSAGE_MAP()
public:
	virtual void DrawItem(LPDRAWITEMSTRUCT /*lpDrawItemStruct*/);
	virtual void MeasureItem(LPMEASUREITEMSTRUCT /*lpMeasureItemStruct*/);
	virtual int CompareItem(LPCOMPAREITEMSTRUCT /*lpCompareItemStruct*/);
	virtual void DeleteItem(LPDELETEITEMSTRUCT lpDeleteItemStruct);
};


