#pragma once

/************************************************************************/
/*不同的定时类型                                                        */
/************************************************************************/
typedef enum tagENUM_TIMETYPE{
	TIMETYPE_ALARM=0,						//普通闹钟提醒,弹出一个对话框提醒，并显示相关提醒信息
	TIMETYPE_SHUTDOWN,						//自动关机
	TIMETYPE_LOGOFF,						//自动注销
	TIMETYPE_RESTART,						//自动重启
	TIMETYPE_ALARMONSHUT,					//关机时提醒
	TIMETYPE_ALARMONSTART					//开机时提醒
} ENUM_TIMETYPE;

/************************************************************************/
/*不同的定时频率(定时的间隔时间)                                        */
/************************************************************************/
typedef enum tagENUM_TIMEFREQ 
{
	TIMEFREQ_WEEK=0,						//每周
	TIMEFREQ_WEEKDAY,						//工作日
	TIMEFREQ_DAY,							//每天
	TIMEFREQ_HOUR,							//每小时
} ENUM_TIMEFREQ;

class CTimeObject : public CObject
{
public:
	CTimeObject();
	CTimeObject(const CTime time, const UINT timeLazyMinute,
				const ENUM_TIMEFREQ timeFreq, const ENUM_TIMETYPE timeType, 
				const CString info,
				const UINT timeCount=0);
	virtual ~CTimeObject();

	BOOL IsOnTime(CTime currentTime);
	void UpdateNextTime();
	void ResponseOnTime(CTime currentTime, UINT nIndex);
	void ResponseOnStartOrShut(CTime currentTime, UINT nIndex);
	CString ToString();

public:
	CTime m_time;					//定时响应时间
	UINT m_timeLazyMinute;			//懒人延后分钟
	ENUM_TIMETYPE m_timeType;		//定时类型
	ENUM_TIMEFREQ m_timeFreq;		//定时频率
	CString m_info;					//定时响应信息
	UINT m_timeCount;				//定时已经重复过的次数
};


