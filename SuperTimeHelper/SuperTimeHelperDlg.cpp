
// SuperTimeHelperDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "SuperTimeHelper.h"
#include "SuperTimeHelperDlg.h"
#include <shellapi.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// 用于应用程序“关于”菜单项的 CAboutDlg 对话框

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// 对话框数据
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

// 实现
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CSuperTimeHelperDlg 对话框




CSuperTimeHelperDlg::CSuperTimeHelperDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSuperTimeHelperDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);

	m_bIsToTray = FALSE;//最开始没有托盘化
}

void CSuperTimeHelperDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_TIMELIST, m_timeListBox);
	DDX_Control(pDX, IDB_ADD, m_btnAdd);
	DDX_Control(pDX, IDB_EDIT, m_btnEdit);
	DDX_Control(pDX, IDB_DEL, m_btnDel);
	DDX_Control(pDX, IDB_COPY, m_btnCopy);
}

BEGIN_MESSAGE_MAP(CSuperTimeHelperDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDB_ADD, &CSuperTimeHelperDlg::OnBnClickedAdd)
	ON_BN_CLICKED(IDB_EDIT, &CSuperTimeHelperDlg::OnBnClickedEdit)
	ON_BN_CLICKED(IDB_DEL, &CSuperTimeHelperDlg::OnBnClickedDel)
	ON_BN_CLICKED(IDB_COPY, &CSuperTimeHelperDlg::OnBnClickedCopy)
	ON_WM_TIMER()
	ON_LBN_DBLCLK(IDC_TIMELIST, &CSuperTimeHelperDlg::OnLbnDblclkTimelist)
	ON_WM_QUERYENDSESSION()
	ON_MESSAGE (WM_NOTIFYICON ,&OnNotifyIcon)
	ON_WM_HOTKEY()
	ON_COMMAND(IDM_MAIN, &CSuperTimeHelperDlg::OnMain)
	ON_COMMAND(IDM_EXIT, &CSuperTimeHelperDlg::OnExit)
	ON_COMMAND(IDM_HOTKEY, &CSuperTimeHelperDlg::OnHotkey)
	ON_COMMAND(IDM_AUTORUN, &CSuperTimeHelperDlg::OnAutorun)
	ON_COMMAND(IDM_HIDE, &CSuperTimeHelperDlg::OnHide)
	ON_COMMAND(IDM_WEBSITE, &CSuperTimeHelperDlg::OnWebsite)
	ON_COMMAND(IDM_ABOUT, &CSuperTimeHelperDlg::OnAbout)
	ON_WM_INITMENUPOPUP()
	ON_COMMAND(IDM_READ, &CSuperTimeHelperDlg::OnRead)
	ON_COMMAND(IDM_EXPORT, &CSuperTimeHelperDlg::OnExport)
	ON_WM_NCPAINT()
END_MESSAGE_MAP()


// CSuperTimeHelperDlg 消息处理程序

BOOL CSuperTimeHelperDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// 将“关于...”菜单项添加到系统菜单中。

	// IDM_ABOUTBOX 必须在系统命令范围内。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 设置此对话框的图标。当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	// TODO: 在此添加额外的初始化代码
	/************************************************************************/
	/*初始化读取参数和设置参数,对于部分参数需要
	  处理一下得到对应的界面参数和内存参数                                  */
	/************************************************************************/
	m_configIni = JWIni(theApp.m_csConfigFileName.GetBuffer(MAX_PATH));
	CTime timeCurrent = CTime::GetCurrentTime();

	m_bAutoRun = m_configIni.GetInt(TEXT("Setting"), TEXT("Autorun"), 1);

	m_bHotKey_Shift =  m_configIni.GetInt(TEXT("Setting"), TEXT("Hotkey_Shift"), 1);
	m_bHotKey_Ctrl =  m_configIni.GetInt(TEXT("Setting"), TEXT("Hotkey_Ctrl"), 1);
	m_bHotKey_Alt =  m_configIni.GetInt(TEXT("Setting"), TEXT("Hotkey_Alt"), 1);
	m_nHotkey_Key =  m_configIni.GetInt(TEXT("Setting"), TEXT("Hotkey_Key"), 66);
	m_nSetModifier = 0x0000;
	m_wShowModifier = 0x00;

	//组合得到参数
	if(TRUE == m_bHotKey_Shift)
	{
		m_nSetModifier |= MOD_SHIFT;
		m_wShowModifier |= HOTKEYF_SHIFT;
	}
	if(TRUE == m_bHotKey_Ctrl)
	{
		m_nSetModifier |= MOD_CONTROL;
		m_wShowModifier |= HOTKEYF_CONTROL;
	}
	if(TRUE == m_bHotKey_Alt)
	{
		m_nSetModifier |= MOD_ALT;
		m_wShowModifier |= HOTKEYF_ALT;
	}

	m_bHide = m_configIni.GetInt(TEXT("Setting"), TEXT("Hide"), 0);

	/************************************************************************/
	/* 初始化界面                                                           */
	/************************************************************************/
	m_mainMenu.LoadMenu(IDR_MAINMENU);
	SetMenu(&m_mainMenu);
	m_trayMenu.LoadMenu(IDR_TRAY_MENU);

	//初始化按钮
	m_btnAdd.SetIcon(IDI_ADD);
	m_btnAdd.SetAlign(CButtonST::ST_ALIGN_HORIZ);
	m_btnAdd.SetTooltipText(TEXT("添加一个定时"));
	m_btnAdd.DrawBorder(FALSE);
	m_btnEdit.SetIcon(IDI_EDIT);
	m_btnEdit.SetAlign(CButtonST::ST_ALIGN_HORIZ);
	m_btnEdit.SetTooltipText(TEXT("编辑选中定时"));
	m_btnEdit.DrawBorder(FALSE);
	m_btnDel.SetIcon(IDI_DELETE);
	m_btnDel.SetAlign(CButtonST::ST_ALIGN_HORIZ);
	m_btnDel.SetTooltipText(TEXT("删除选中定时"));
	m_btnDel.DrawBorder(FALSE);
	m_btnCopy.SetIcon(IDI_COPY);
	m_btnCopy.SetAlign(CButtonST::ST_ALIGN_HORIZ);
	m_btnCopy.SetTooltipText(TEXT("复制选中定时"));
	m_btnCopy.DrawBorder(FALSE);

	//读入定时数据
	m_timeListBox.ReadFromFile(m_configIni);

	/************************************************************************/
	/* 初始化操作                                                           */
	/************************************************************************/
	//是否显示到后台运行
	if (TRUE == m_bHide)
	{
		ShowWindow(SW_HIDE);
	}
	else
	{
		//托盘化
		ToTray();
	}

	//保证用户第一次使用时加入开机启动
	if (m_bAutoRun == TRUE)
	{
		SetAutoRun();
	}

	//设置全局热键
	m_atom = GlobalAddAtom(TEXT("SUPERTIMEHELPER_JIMWEN"));
	if (FALSE == RegisterHotKey(m_hWnd, m_atom, m_nSetModifier, m_nHotkey_Key))
	{
		MessageBox(TEXT("设置热键失败，可能产生冲突，请换其它热键!"));
	}

	//开机提醒
	if (m_timeListBox.m_pStartObject != NULL)
	{
		m_timeListBox.m_pStartObject->ResponseOnStartOrShut(CTime::GetCurrentTime(), m_timeListBox.FindTheTypeIndex(TIMETYPE_ALARMONSTART));
	}

	//调整关机顺序
	SetProcessShutdownParameters(0x4fe, SHUTDOWN_NORETRY);//提升到最先关闭

	//开启计时	
	SetTimer(TIMER_1, 1000, NULL);

	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

void CSuperTimeHelperDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CSuperTimeHelperDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR CSuperTimeHelperDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


void CSuperTimeHelperDlg::OnDestroy()
{
	//清除相关资源
	GlobalDeleteAtom(m_atom);

	//退出时强制删除托盘图标,防止出现程序退出托盘图标还在
	DeleteTray();

	CDialog::OnDestroy();
}

/************************************************************************/
/* 添加、删除、编辑、复制定时                                           */
/************************************************************************/
void CSuperTimeHelperDlg::OnBnClickedAdd()
{
	// TODO: 在此添加控件通知处理程序代码
	CDlgAdd dlgAdd(TEXT("添加定时"), 10);
	
	if (IDOK == dlgAdd.DoModal())
	{
		m_timeListBox.AddObject(CTime(dlgAdd.m_date.GetYear(),
									  dlgAdd.m_date.GetMonth(),
									  dlgAdd.m_date.GetDay(),
									  dlgAdd.m_time.GetHour(),
									  dlgAdd.m_time.GetMinute(),
									  dlgAdd.m_time.GetSecond()),
							    dlgAdd.m_timeLazyMinute,
								(ENUM_TIMEFREQ)dlgAdd.m_timeFreq,
								(ENUM_TIMETYPE)dlgAdd.m_timeType,
								dlgAdd.m_timeInfo);

		m_timeListBox.ExportToFile(m_configIni);
	}
}

void CSuperTimeHelperDlg::OnBnClickedEdit()
{
	// TODO: 在此添加控件通知处理程序代码
	if (m_timeListBox.GetCount() == 0 || LB_ERR == m_timeListBox.GetCurSel())
	{
		MessageBox(TEXT("请先选中要编辑的定时!"));
		return;
	}

	UINT nIndex;
	nIndex = LB_ERR == m_timeListBox.GetCurSel() ? (m_timeListBox.SetCurSel(0), 0) : m_timeListBox.GetCurSel();

	CTimeObject *pListDataItem = (CTimeObject *)m_timeListBox.GetItemData(nIndex);
	CDlgAdd dlgAdd(TEXT("编辑定时"), pListDataItem);


	if (IDOK == dlgAdd.DoModal())
	{

		m_timeListBox.ModifyObject( m_timeListBox.GetCurSel(), 
									CTime(dlgAdd.m_date.GetYear(),
									dlgAdd.m_date.GetMonth(),
									dlgAdd.m_date.GetDay(),
									dlgAdd.m_time.GetHour(),
									dlgAdd.m_time.GetMinute(),
									dlgAdd.m_time.GetSecond()),
									dlgAdd.m_timeLazyMinute,
									(ENUM_TIMEFREQ)dlgAdd.m_timeFreq,
									(ENUM_TIMETYPE)dlgAdd.m_timeType,
									dlgAdd.m_timeInfo,
									pListDataItem->m_timeCount);

		m_timeListBox.ExportToFile(m_configIni);
	}
}

void CSuperTimeHelperDlg::OnLbnDblclkTimelist()
{
	// TODO: 在此添加控件通知处理程序代码
	OnBnClickedEdit();
}

void CSuperTimeHelperDlg::OnBnClickedDel()
{
	// TODO: 在此添加控件通知处理程序代码
	if (m_timeListBox.GetCount() == 0 || LB_ERR == m_timeListBox.GetCurSel())
	{
		MessageBox(TEXT("请先选中要删除的定时!"));
		return;
	}

	m_timeListBox.DeleteObject(m_timeListBox.GetCurSel());
	m_timeListBox.ExportToFile(m_configIni);
}

void CSuperTimeHelperDlg::OnBnClickedCopy()
{
	// TODO: 在此添加控件通知处理程序代码
	if (m_timeListBox.GetCount() == 0 || LB_ERR == m_timeListBox.GetCurSel())
	{
		MessageBox(TEXT("请先选中要复制的定时!"));
		return;
	}

	UINT nIndex;
	nIndex = LB_ERR == m_timeListBox.GetCurSel() ? (m_timeListBox.SetCurSel(0), 0) : m_timeListBox.GetCurSel();

	CTimeObject *pListDataItem = (CTimeObject *)m_timeListBox.GetItemData(nIndex);
	CDlgAdd dlgAdd(TEXT("复制定时"), pListDataItem);

	if (IDOK == dlgAdd.DoModal())
	{
		m_timeListBox.AddObject(CTime(dlgAdd.m_date.GetYear(),
								dlgAdd.m_date.GetMonth(),
								dlgAdd.m_date.GetDay(),
								dlgAdd.m_time.GetHour(),
								dlgAdd.m_time.GetMinute(),
								dlgAdd.m_time.GetSecond()),
								dlgAdd.m_timeLazyMinute,
								(ENUM_TIMEFREQ)dlgAdd.m_timeFreq,
								(ENUM_TIMETYPE)dlgAdd.m_timeType,
								dlgAdd.m_timeInfo);

		m_timeListBox.ExportToFile(m_configIni);
	}
}

/************************************************************************/
/* 定时响应                                                             */
/************************************************************************/
void CSuperTimeHelperDlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	int iCount;
	CTime curTime = CTime::GetCurrentTime();
	CTimeObject* pObject;

	if (TIMER_1 == nIDEvent)
	{
		if ((iCount = m_timeListBox.GetCount()) <= 0)
		{
			return;
		}

		//循环判断当前的时间事件是否被触发
		for (int i = 0; i<iCount; i++ )
		{
			pObject = (CTimeObject *)(m_timeListBox.GetItemData(i));
			
			pObject->ResponseOnTime(curTime, i);
		}
	}

	CDialog::OnTimer(nIDEvent);
}

/************************************************************************/
/* 关机时提醒                                                           */
/************************************************************************/
BOOL CSuperTimeHelperDlg::OnQueryEndSession()
{
	if (!CDialog::OnQueryEndSession())
		return FALSE;

	// TODO:  在此添加专用的查询结束会话代码
	if (m_timeListBox.m_pShutObject != NULL)
	{
		m_timeListBox.m_pShutObject->ResponseOnStartOrShut(CTime::GetCurrentTime(), m_timeListBox.FindTheTypeIndex(TIMETYPE_ALARMONSHUT));
	}

	return TRUE;
}

/************************************************************************/
/* 托盘操作                                                             */
/************************************************************************/
BOOL CSuperTimeHelperDlg::ToTray()
{
	//当前已经托盘化了直接表示成功
	if (TRUE == m_bIsToTray)
	{
		return TRUE;
	}

	//设置托盘设置结构体参数
	m_nid.cbSize = sizeof(NOTIFYICONDATA);									//托盘设置结构体大小
	m_nid.hWnd = this->GetSafeHwnd();										//设置图盘图标对应的窗口句柄
	m_nid.uID = IDR_MAINFRAME;												//托盘图标ID
	m_nid.hIcon = LoadIcon(	AfxGetInstanceHandle(), MAKEINTRESOURCE(IDR_MAINFRAME));//托盘图标句柄
	m_nid.uFlags = /*NIF_INFO|*/NIF_ICON|NIF_TIP|NIF_MESSAGE;					//托盘显示气泡，图标，鼠标悬停提示,接受托盘消息	
	m_nid.uCallbackMessage = WM_NOTIFYICON;								//设置操作托盘图标时的消息
	m_nid.uTimeout= 500;													//设置托盘化时的提示信息(m_nid.szInfo)的显示时间
	m_nid.dwInfoFlags = NIIF_USER;											//设置气泡框的图标
	lstrcpy(m_nid.szTip,   TEXT("左键双击显示设置对话框\n右键单击显示菜单"));							//设置鼠标悬停提示
	lstrcpy(m_nid.szInfo,   TEXT("左键双击显示设置对话框,右键单击显示菜单"));							//设置托盘化时的提示信息(m_nid.szInfo)
	lstrcpy(m_nid.szInfoTitle,   TEXT("提示"));								//设置托盘化时的提示信息(m_nid.szInfo)的标题

	//在托盘区添加图标
	if(::Shell_NotifyIcon(NIM_ADD, &m_nid)) 									
	{
		ShowWindow(SW_HIDE);							//创建托盘图标成功则隐藏主窗口
		m_bIsToTray = TRUE;

		return TRUE ;
	}
	else
	{
		MessageBox(TEXT("创建托盘图标失败"), TEXT("错误"), MB_OK) ;
		return FALSE ;
	}
}

BOOL CSuperTimeHelperDlg::DeleteTray()
{
	//当前已经删除托盘化了表示删除成功
	if (FALSE == m_bIsToTray)
	{
		return TRUE;
	}

	//删除托盘图标
	if(::Shell_NotifyIcon(NIM_DELETE, &m_nid)) 									
	{
		m_bIsToTray = FALSE;
		return TRUE ;
	}
	else
	{
		MessageBox(TEXT("删除托盘图标失败"), TEXT("错误"), MB_OK) ;
		return FALSE ;
	}
}

LRESULT CSuperTimeHelperDlg::OnNotifyIcon( WPARAM wParam, LPARAM lParam )
{
	switch(lParam)//根据lParam判断相对应的事件
	{
	case WM_LBUTTONDBLCLK://如果左键双击托盘图标,则显示主窗体
		OnMain();
		break;
	case WM_RBUTTONUP://如果右键菜单弹起,则弹出菜单
		CPoint pos;
		GetCursorPos(&pos);
		CMenu *m_pMenu = m_trayMenu.GetSubMenu(0);

		if(m_pMenu != NULL)
		{
			SetForegroundWindow();//加这句是为了鼠标点击其他地方时,弹出的菜单能够消失
			m_pMenu->TrackPopupMenu(TPM_RIGHTBUTTON|TPM_RIGHTALIGN, pos.x+1, pos.y+1, this);
		}
		break;
	}

	return 0;
}

/************************************************************************/
/* 开机自启动操作                                                       */
/************************************************************************/
BOOL CSuperTimeHelperDlg::SetAutoRun()
{
	CRegKey regKey;
	if(regKey.Open(HKEY_LOCAL_MACHINE, TEXT("Software\\Microsoft\\Windows\\CurrentVersion\\Run")) == ERROR_SUCCESS)
	{
		if (ERROR_SUCCESS == regKey.SetValue(TEXT("Jimwen-SuperTimeHelper"), REG_SZ, theApp.m_csExeFileName.GetBuffer(256), (theApp.m_csExeFileName.GetLength()+1)*sizeof(TCHAR)))//注意这里最后一个参数是nBits,且要求size算上terminating null character
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}

		regKey.Close();
	}
	else
	{
		return FALSE;
	}
}

BOOL CSuperTimeHelperDlg::CancelAutoRun()
{
	CRegKey regKey;
	if(regKey.Open(HKEY_LOCAL_MACHINE, TEXT("Software\\Microsoft\\Windows\\CurrentVersion\\Run")) == ERROR_SUCCESS)
	{
		if (ERROR_SUCCESS == regKey.DeleteValue(TEXT("Jimwen-SuperTimeHelper")))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}

		regKey.Close();
	}
	else
	{
		return FALSE;
	}
}

/************************************************************************/
/* 热键响应                                                             */
/************************************************************************/
void CSuperTimeHelperDlg::OnHotKey(UINT nHotKeyId, UINT nKey1, UINT nKey2)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	OnMain();//主窗口显示

	CDialog::OnHotKey(nHotKeyId, nKey1, nKey2);
}

/************************************************************************/
/* 初始化弹出菜单                                                       */
/************************************************************************/
void CSuperTimeHelperDlg::OnInitMenuPopup(CMenu* pPopupMenu, UINT nIndex, BOOL bSysMenu)
{
	//CDialog::OnInitMenuPopup(pPopupMenu, nIndex, bSysMenu);

	// TODO: 在此处添加消息处理程序代码

	if ((m_mainMenu.GetSubMenu(2)->GetSafeHmenu()) == pPopupMenu->GetSafeHmenu() || 
		(m_trayMenu.GetSubMenu(0)->GetSafeHmenu()) == pPopupMenu->GetSafeHmenu())
	{
		pPopupMenu->CheckMenuItem(IDM_AUTORUN, m_bAutoRun == TRUE ? MF_CHECKED : MF_UNCHECKED);
		pPopupMenu->CheckMenuItem(IDM_HIDE, m_bHide == TRUE ? MF_CHECKED : MF_UNCHECKED);
	}
}

/************************************************************************/
/* 菜单响应                                                             */
/************************************************************************/
void CSuperTimeHelperDlg::OnMain()
{
	// TODO: 在此添加命令处理程序代码
	ShowWindow(SW_SHOWNORMAL);
	SetForegroundWindow();
}

void CSuperTimeHelperDlg::OnExit()
{
	//自己new的对话框必须自己Destroy
	DestroyWindow();
}

void CSuperTimeHelperDlg::OnHotkey()
{
	// TODO: 在此添加命令处理程序代码
	CDlgHotkey dlgHotkey(m_nHotkey_Key, m_wShowModifier);

	if (IDOK == dlgHotkey.DoModal())
	{
		//实际热键值对应到内存参数
		m_nSetModifier = 0x0000;
		m_wShowModifier = dlgHotkey.m_wModifer;
		m_nHotkey_Key = dlgHotkey.m_nHotkey;
		if(m_wShowModifier & HOTKEYF_SHIFT)
		{
			m_bHotKey_Shift = 1;
			m_nSetModifier |= MOD_SHIFT;
		}
		else
		{
			m_bHotKey_Shift = 0;
		}
		if (m_wShowModifier & HOTKEYF_CONTROL)
		{
			m_bHotKey_Ctrl = 1;
			m_nSetModifier |= MOD_CONTROL;
		}
		else
		{
			m_bHotKey_Ctrl = 0;
		}
		if (m_wShowModifier & HOTKEYF_ALT)
		{
			m_bHotKey_Alt = 1;
			m_nSetModifier |= MOD_ALT;
		}
		else
		{
			m_bHotKey_Alt = 0;
		}

		//设置配置文件参数
		m_configIni.SetInt(TEXT("Setting"), TEXT("Hotkey_Shift"), m_bHotKey_Shift);
		m_configIni.SetInt(TEXT("Setting"), TEXT("Hotkey_Ctrl"), m_bHotKey_Ctrl);
		m_configIni.SetInt(TEXT("Setting"), TEXT("Hotkey_Alt"), m_bHotKey_Alt);
		m_configIni.SetInt(TEXT("Setting"), TEXT("Hotkey_Key"), m_nHotkey_Key);

		//重新设置热键
		UnregisterHotKey(m_hWnd, m_atom);
		if (FALSE == RegisterHotKey(m_hWnd, m_atom, m_nSetModifier, m_nHotkey_Key))
		{
			MessageBox(TEXT("设置热键失败，可能产生冲突，请换其它热键!"));
		}
	}
}

void CSuperTimeHelperDlg::OnAutorun()
{
	// TODO: 在此添加命令处理程序代码
	//翻转状态
	m_bAutoRun = !m_bAutoRun;

	//保存到配置文件
	m_configIni.SetInt(TEXT("Setting"), TEXT("Autorun"), m_bAutoRun);

	//翻转显示
	m_mainMenu.GetSubMenu(2)->CheckMenuItem(IDM_AUTORUN, m_bAutoRun == TRUE ? MF_CHECKED : MF_UNCHECKED);
	m_trayMenu.GetSubMenu(0)->CheckMenuItem(IDM_AUTORUN, m_bAutoRun == TRUE ? MF_CHECKED : MF_UNCHECKED);

	//处理开机启动项
	if (TRUE == m_bAutoRun)
	{
		SetAutoRun();
	}
	else
	{
		CancelAutoRun();
	}
}

void CSuperTimeHelperDlg::OnHide()
{
	// TODO: 在此添加命令处理程序代码
	//翻转状态
	m_bHide = !m_bHide;

	//保存到配置文件
	m_configIni.SetInt(TEXT("Setting"), TEXT("Hide"), m_bHide);

	//翻转显示
	m_mainMenu.GetSubMenu(2)->CheckMenuItem(IDM_HIDE, m_bHide == TRUE ? MF_CHECKED : MF_UNCHECKED);
	m_trayMenu.GetSubMenu(0)->CheckMenuItem(IDM_HIDE, m_bHide == TRUE ? MF_CHECKED : MF_UNCHECKED);

	//是否显示到后台运行
	if (TRUE == m_bHide)
	{
		DeleteTray();
		ShowWindow(SW_HIDE);
	}
	else
	{
		//托盘化
		ToTray();
	}
}

void CSuperTimeHelperDlg::OnWebsite()
{
	// TODO: 在此添加命令处理程序代码
	::ShellExecute(m_hWnd, _T("open"), TEXT("http://jimwen.net/tag.php?tag=SuperTimeHelper"), NULL, NULL, SW_SHOWNORMAL);
}

void CSuperTimeHelperDlg::OnAbout()
{
	// TODO: 在此添加命令处理程序代码
	CAboutDlg dlgAbout;
	dlgAbout.DoModal();
}
void CSuperTimeHelperDlg::OnRead()
{
	// TODO: 在此添加命令处理程序代码
	CFileDialog fileDlg(TRUE);
	fileDlg.m_ofn.lpstrFilter = TEXT("Config File(*.ini)\0*.ini\0\0");
	fileDlg.m_ofn.lpstrDefExt = TEXT("ini");
	fileDlg.m_ofn.lpstrTitle = TEXT("从文件导入定时数据");

	if (fileDlg.DoModal() == IDOK)
	{
		JWIni csIni(fileDlg.GetPathName());
		m_timeListBox.ReadFromFile(csIni);
	}
}

void CSuperTimeHelperDlg::OnExport()
{
	// TODO: 在此添加命令处理程序代码
	CFileDialog fileDlg(FALSE);
	fileDlg.m_ofn.lpstrFilter = TEXT("Config File(*.ini)\0*.ini\0\0");
	fileDlg.m_ofn.lpstrDefExt = TEXT("ini");
	fileDlg.m_ofn.lpstrTitle = TEXT("导出定时数据到文件");

	if (fileDlg.DoModal() == IDOK)
	{
		JWIni csIni(fileDlg.GetPathName());
		m_timeListBox.ExportToFile(csIni);
	}
}

void CSuperTimeHelperDlg::OnNcPaint()
{
	// TODO: 在此处添加消息处理程序代码
	// 不为绘图消息调用 CDialog::OnNcPaint()

	//程序一启动就隐藏对话框,只运行这一次
	static BOOL bISHideWindow = TRUE;
	if (TRUE == bISHideWindow)
	{
		ShowWindow(SW_HIDE);
		bISHideWindow = FALSE;
	}

	CDialog::OnNcPaint();
}
