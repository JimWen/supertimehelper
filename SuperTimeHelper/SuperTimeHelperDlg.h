
// SuperTimeHelperDlg.h : 头文件
//

#pragma once
#include "afxwin.h"
#include "TimeListBox.h"
#include "DlgAdd.h"
#include "DlgHotkey.h"

#define WM_NOTIFYICON WM_USER+1
#define TIMER_1 1


// CSuperTimeHelperDlg 对话框
class CSuperTimeHelperDlg : public CDialog
{

public:

	//配置参数
	BOOL m_bHotKey_Shift;
	BOOL m_bHotKey_Ctrl;
	BOOL m_bHotKey_Alt;
	INT m_nHotkey_Key;
	UINT m_nSetModifier;
	BOOL m_bAutoRun;
	BOOL m_bHide;

	WORD m_wShowModifier;

	JWIni m_configIni;

	//托盘图标
	BOOL ToTray();
	BOOL DeleteTray();
	LRESULT OnNotifyIcon(WPARAM wParam, LPARAM lParam);

	//注册表
	BOOL SetAutoRun();
	BOOL CancelAutoRun();

	CSuperTimeHelperDlg(CWnd* pParent = NULL);	// 标准构造函数

private:
	NOTIFYICONDATA m_nid;
	BOOL m_bIsToTray;
	WORD m_atom;
	CMenu m_mainMenu;
	CMenu m_trayMenu;
	TCHAR m_szBuffer[10];

// 对话框数据
	enum { IDD = IDD_SUPERTIMEHELPER_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持
		

// 实现
protected:
	HICON m_hIcon;

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	CTimeListBox m_timeListBox;
	afx_msg void OnDestroy();
	CButtonST m_btnAdd;
	CButtonST m_btnEdit;
	CButtonST m_btnDel;
	CButtonST m_btnCopy;
	afx_msg void OnBnClickedAdd();
	afx_msg void OnBnClickedEdit();
	afx_msg void OnBnClickedDel();
	afx_msg void OnBnClickedCopy();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnLbnDblclkTimelist();
	afx_msg BOOL OnQueryEndSession();
	afx_msg void OnHotKey(UINT nHotKeyId, UINT nKey1, UINT nKey2);
	afx_msg void OnMain();
	afx_msg void OnExit();
	afx_msg void OnHotkey();
	afx_msg void OnAutorun();
	afx_msg void OnHide();
	afx_msg void OnWebsite();
	afx_msg void OnAbout();
	afx_msg void OnInitMenuPopup(CMenu* pPopupMenu, UINT nIndex, BOOL bSysMenu);
	afx_msg void OnRead();
	afx_msg void OnExport();
	afx_msg void OnNcPaint();
};
