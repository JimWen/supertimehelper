#pragma once

#include "TimeObject.h"

// CDlgAdd 对话框

class CDlgAdd : public CDialog
{
	DECLARE_DYNAMIC(CDlgAdd)

public:
	CDlgAdd(CWnd* pParent = NULL);   // 标准构造函数
	CDlgAdd(CString wndCaption, 
		    CTime date, 
			CTime time, 
			int timeLazyMinute=10,
			int timeType=0, 
			int timeFreq=0, 
			CString timeInfo=TEXT(""),
			CWnd* pParent = NULL);
	CDlgAdd(CString wndCaption,
			int timeLazyMinute=10,
			int timeType=0, 
			int timeFreq=0, 
			CString timeInfo=TEXT(""),
			CWnd* pParent = NULL);
	CDlgAdd(CString wndCaption, const CTimeObject *pObject, CWnd* pParent = NULL);
	void InitControlState();
	virtual ~CDlgAdd();

// 对话框数据
	enum { IDD = IDD_ADD };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	CString m_windowCaption;
	CTime m_date;
	CTime m_time;
	int m_timeType;
	int m_timeFreq;
	CString m_timeInfo;
	virtual BOOL OnInitDialog();
	int m_timeLazyMinute;
	afx_msg void OnCbnSelchangeTimetype();
	afx_msg void OnBnClickedOk();
};
